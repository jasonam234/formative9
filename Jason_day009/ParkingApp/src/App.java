import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;

/**
 * Simple parking app
 * @author Jason Adrian Mahalim
 * @version 1.0
 * @since 07/10/2021
 */

public class App {
    static Scanner scanner = new Scanner(System.in);
    static ArrayList<ParkingSpot> parkingSpot = new ArrayList<ParkingSpot>();
    static LocalDateTime localDateTime = LocalDateTime.now();
    static DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static void main(String[] args) throws Exception {
        int option;
        int parkingSpace = 0;
        boolean emptyMarker= true;
        long totalHour;
        long ticketPrice;
        long totalMinute;
        String carNumber;

        createFile();
        while(true){
            clearScreen();
            System.out.println("===============================================");
            System.out.println("Parking Application");
            System.out.println("===============================================");
            System.out.println("1. Create parking space");
            System.out.println("2. Push a car");
            System.out.println("3. Pop a car");
            System.out.println("4. Information");
            System.out.println("0. Exit ");
            System.out.print("Input : ");
    
            option = scanner.nextInt();
            scanner.nextLine();

            if(option == 1){
                if(parkingSpace == 0){
                    clearScreen();
                    System.out.println("===============================================");
                    System.out.println("Parking Application");
                    System.out.println("===============================================");
                    System.out.print("Set parking space : ");
                    parkingSpace = scanner.nextInt();
                    scanner.nextLine();
                    fileWriter("Created " + parkingSpace + " parking space");
                    System.out.println("Parking space created");

                    for(int i = 0; i < parkingSpace; i++){
                        parkingSpot.add(new ParkingSpot(i+1,  "", ""));
                    }

                    parkingSpot.set(0, new ParkingSpot(1, "A-1111-AAA", "2021-10-07T13:00:00"));
                    parkingSpot.set(1, new ParkingSpot(2, "B-2222-BBB", localDateTime.minusHours(3).toString()));
                    parkingSpot.set(3, new ParkingSpot(4, "C-3333-CCC", localDateTime.now().toString()));
                    parkingSpot.set(4, new ParkingSpot(5, "D-4444-DDD", localDateTime.minusHours(30).toString()));
                    parkingSpot.set(5, new ParkingSpot(6, "E-5555-EEE", localDateTime.minusHours(1).toString()));
                    parkingSpot.set(6, new ParkingSpot(7, "F-6666-FFF", "2021-10-07T12:00:00"));
                    emptyMarker = false;
                    pause();
                }else if(parkingSpace > 0){
                    System.out.println("Parking space already set");
                    pause();
                }
            }else if(option == 2){
                clearScreen();
                System.out.println("===============================================");
                System.out.println("Parking Application");
                System.out.println("===============================================");
                if(emptyMarker == true){System.out.println("Parking space is not exist"); pause(); continue;}

                if(parkingSpot.size() <= parkingSpace){
                    System.out.print("Car number : ");
                    carNumber = scanner.nextLine();

                    Pattern pattern = Pattern.compile("[A-Z]{1}-[0-9]{4}-[A-Z]{3}$");
                    Matcher matcher = pattern.matcher(carNumber);
                    if(matcher.find()){
                        for(int i = 0; i <= parkingSpot.size(); i++){
                            if(i == parkingSpot.size()){
                                System.out.println("Parking space is full");
                                fileWriter("Car " + carNumber + " wanted to park, but parking is full");
                            }else if(parkingSpot.get(i).getCarNumber().isEmpty()){
                                parkingSpot.set(i, new ParkingSpot(i+1, carNumber, localDateTime.now().toString()));
                                fileWriter("Car " + parkingSpot.get(i).getCarNumber() + " registered to slot " + parkingSpot.get(i).getSpot());
                                break;
                            }
                        }
                        pause();
                    }else{
                        System.out.println("Invalid car number");
                        pause();
                    }
                }
            }else if(option == 3){
                clearScreen();
                System.out.println("===============================================");
                System.out.println("Parking Application");
                System.out.println("===============================================");
                if(emptyMarker == true){System.out.println("Parking space is not exist"); pause(); continue;}

                System.out.print("Enter car number : ");
                carNumber = scanner.nextLine();

                for(int i = 0; i <= parkingSpot.size(); i++){
                    if(i == parkingSpot.size()){
                        System.out.println("Car not found");
                        fileWriter("Car " + carNumber + " not found");
                        pause();
                    }else if(carNumber.equals(parkingSpot.get(i).getCarNumber())){
                        ticketPrice = 0;
                        LocalDateTime timeIn = LocalDateTime.parse(parkingSpot.get(i).getTime());

                        totalHour = ChronoUnit.HOURS.between(timeIn, localDateTime);
                        totalMinute = ChronoUnit.MINUTES.between(timeIn, localDateTime);
                        System.out.println("Total hour(s) : " + totalHour);
                        System.out.println("Total minutes(s) : " + totalMinute);

                        if(totalHour < 2){
                            ticketPrice = 10_000;
                        }else if(totalHour == 2 && totalMinute > 120){
                            ticketPrice = 20_000;
                        }else if(totalMinute >= 120){
                            totalHour = totalHour - 1;
                            System.out.println(totalHour);
                            ticketPrice = 10_000 + 10_000 * totalHour;
                        }
                        fileWriter("Car " + carNumber + " is out from slot " + parkingSpot.get(i).getSpot() + " total ticket price : " + ticketPrice);
                        parkingSpot.set(i, new ParkingSpot(i+1, "", ""));
                        
                        pause();
                        break;
                    }else if(!carNumber.equals(parkingSpot.get(i).getCarNumber())){
                        continue;
                    }
                }
                
            }else if(option == 4){
                clearScreen();
                System.out.println("===============================================");
                System.out.println("Parking Application");
                System.out.println("===============================================");
                if(emptyMarker == true){System.out.println("Parking space is not exist"); pause(); continue;}
                System.out.println("No.    Car Number     Time In            ");
                for(int i = 0; i < parkingSpot.size(); i++){
                    if(parkingSpot.get(i).getTime().equals("")){
                        System.out.println(parkingSpot.get(i).getSpot());
                    }else{
                        System.out.println(parkingSpot.get(i).getSpot() + "      " + parkingSpot.get(i).getCarNumber() + "     " + LocalDateTime.parse(parkingSpot.get(i).getTime()).format(customFormatter));
                    }
                }
                pause();
            }else if(option == 0){
                break;
            }else{
                System.out.print("Invalid input");
            }
        }
    }

    /**
     * Pause for better UX
     */
    public static void pause(){
        System.out.println("Press any key to continue . . .");
        scanner.nextLine();
    }

    /**
     * Clear screen for better UXs
     */
    public static void clearScreen(){
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }

    /**
     * Create new textFile
     */
    public static void createFile(){
        try{
            File file = new File("parkingLog.txt");
            FileWriter writer =  new FileWriter("parkingLog.txt");
            if(file.createNewFile()) {
                System.out.println("File created: " + file.getName());
                writer.write("");
                writer.close();
            }else{
                System.out.println("File already exists.");
                writer.write("");
                writer.close();
            }
          }catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Write to file
     * @param data
     */
    public static void fileWriter(String data){
        try{
            FileWriter writer = new FileWriter("parkingLog.txt",true);
            writer.write(data + "\n");
            writer.close();
        }catch(IOException e){
            System.out.println("Error writing to file");
            pause();
        }
        

    }
}
