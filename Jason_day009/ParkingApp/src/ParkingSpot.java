public class ParkingSpot {
    private int spot;
    private String carNumber;
    private String time;
    
    public ParkingSpot(){}

    public ParkingSpot(int newSpot){
        this.spot = newSpot;
    }

    public ParkingSpot(int newSpot, String newCarNumber, String newTime){
        this.spot = newSpot;
        this.carNumber = newCarNumber;
        this.time = newTime;
    }

    public int getSpot(){
        return this.spot;
    }

    public String getCarNumber(){
        return this.carNumber;
    }

    public String getTime(){
        return this.time;
    }
}
